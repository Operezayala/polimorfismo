package com.ks.otros;


import com.ks.otros.herencia.Autorizador;
import com.ks.otros.herencia.Estructura;
import com.ks.otros.herencia.Konversa;
import com.ks.otros.herencia.Ruteador;

public class App
{
    public static void main(String[] args)
    {
        Konversa VLobjKonversa = new Konversa();
        VLobjKonversa.setConfig("Datos");
        VLobjKonversa.setTienda("Datos");
        VLobjKonversa.setTarjeta("Datos");
        VLobjKonversa.guardar();


        Ruteador VLobjRuteador = new Ruteador();
        VLobjRuteador.setConfig("otro");
        VLobjRuteador.setTarjeta("otro");
        VLobjRuteador.guardar();

        Estructura VLobjSiempre;

        int i = 1;
        String tienda = "Datos";

        switch (i)
        {
            case 1:
                Konversa VL1 = new Konversa();
                VLobjSiempre = VL1.obtenerDatos(tienda);
                break;
            case 2:
                Ruteador VL2 = new Ruteador();
                VLobjSiempre = VL2.obtenerDatos(tienda);
                break;
            default:
                VLobjSiempre = null;
                break;
        }

        if (VLobjSiempre != null)
        {
            System.out.println("Configuracion: " + VLobjSiempre.getConfig());
        }

        i = 2;
        tienda = "otro";

        switch (i)
        {
            case 1:
                Konversa VL1 = new Konversa();
                VLobjSiempre = VL1.obtenerDatos(tienda);
                break;
            case 2:
                Ruteador VL2 = new Ruteador();
                VLobjSiempre = VL2.obtenerDatos(tienda);
                break;
            default:
                VLobjSiempre = null;
                break;
        }

        if (VLobjSiempre != null)
        {
            System.out.println("Configuracion: " + VLobjSiempre.getConfig());
        }



        VLobjSiempre = new Konversa();
        VLobjSiempre = new Autorizador();
        VLobjSiempre = new Ruteador();


    }
}
