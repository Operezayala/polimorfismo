package com.ks.otros.herencia;

import java.util.Hashtable;

/**
 * Created by Miguel on 26/08/2016.
 */
public class Ruteador extends Estructura implements procesar
{
    private static Hashtable <String, Ruteador> VMhasConfig;



    static {
        VMhasConfig = new Hashtable<String, Ruteador>();
    }

    public Ruteador()
    {

    }

    public void guardar()
    {
        VMhasConfig.put(getConfig(),this);
    }

    public Estructura obtenerDatos(String config)
    {
        if (config != null)
        {
            if (VMhasConfig.containsKey(config))
            {
                return VMhasConfig.get(config);
            }
            else
            {
                return  null;
            }
        }
        else
        {
            return null;
        }
    }

    public Estructura obtenerDatos(String config, String tienda)
    {
        return obtenerDatos(config);
    }




}
