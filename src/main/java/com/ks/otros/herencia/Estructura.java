package com.ks.otros.herencia;

import java.util.Hashtable;

/**
 * Created by Miguel on 26/08/2016.
 */
public class Estructura
{
    private String ruta;
    private String id;
    private String config;
    private String procesador;
    private String tarjeta;

    public Estructura ()
    {
        ruta = "";
        id = "";
        config = "";
        procesador = "";
        tarjeta = "";
    }
    public String getRuta()
    {
        return ruta;
    }

    public void setRuta(String ruta)
    {
        this.ruta = ruta;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getConfig()
    {
        return config;
    }

    public void setConfig(String config)
    {
        this.config = config;
    }

    public String getProcesador()
    {
        return procesador;
    }

    public void setProcesador(String procesador)
    {
        this.procesador = procesador;
    }

    public String getTarjeta()
    {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta)
    {
        this.tarjeta = tarjeta;
    }

    protected Estructura buscarHash (String key, Hashtable datos)
    {
        if (key != null)
        {
            if (datos.containsKey(key))
            {
                return (Estructura) datos.get(key);
            }
            else
            {
                return  null;
            }
        }
        else
        {
            return null;
        }
    }

}
