package com.ks.otros.herencia;


import java.util.Hashtable;

/**
 * Created by Miguel on 26/08/2016.
 */
public class Konversa extends Estructura implements procesar
{
    private static Hashtable <String, Konversa> VMhasTienda;
    private static Hashtable <String, Konversa> VMhasConfig;

    private String tienda;

    static {
        VMhasConfig = new Hashtable<String, Konversa>();
        VMhasTienda = new Hashtable<String, Konversa>();
    }

    public Konversa()
    {
        tienda = "";
    }

    public void guardar()
    {
        VMhasConfig.put(getConfig(),this);
        VMhasTienda.put(getTienda(), this);
    }

    public Estructura obtenerDatos(String config)
    {
       return buscarHash(config, VMhasConfig);
    }

    public Estructura obtenerDatos(String config, String tienda)
    {
        if (config == null)
        {
            return buscarHash(tienda, VMhasTienda);
        }
        else
        {
            return obtenerDatos(config);
        }
    }


    public String getTienda()
    {
        return tienda;
    }

    public void setTienda(String tienda)
    {
        this.tienda = tienda;
    }
}
